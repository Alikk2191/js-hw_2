/*Які існують типи даних у Javascript?
У чому різниця між == і ===?
Що таке оператор?
*/

// 1 Типы данных бывают строчные, числовые, булевые, null, undefined, BigInt, object, symbol
// 2 Разница в том что == это не строгое равно где оператор равенства приводит обе величины к общему
//     типу, а === это строгое равно, где сравниваются типы и значения переменных.
// 3 Это простая внутрення функция JS для выполнения определенных функций, например присваивания, сложения и тд

let name = prompt("Enter your name:");
let age = parseInt(prompt("Enter your age:"));

while (!name || isNaN(age)) {
  name = prompt(`Enter your name again (previous value: "${name}"):`);
  age = parseInt(prompt(`Enter your age again (previous value: "${age}"):`));
}

if (age < 18) {
  alert("You are not allowed to visit this website.");
} else if (age >= 18 && age <= 22) {
  if (confirm("Are you sure you want to continue?")) {
    alert(`Welcome, ${name}`);
  } else {
    alert("You are not allowed to visit this website.");
  }
} else {
  alert(`Welcome, ${name}`);
}
